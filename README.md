# PTTMAM (Powertrain Technology Transition Market Agent Model)

Welcome to the repository for the Powertrain Technology Transition Market Agent Model (PTTMAM). This tool is designed to simulate and analyze the market dynamics of different powertrain technologies in the European Union.

## Table of Contents

- [Overview](#overview)
- [Key Features](#key-features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Getting Started](#getting-started)
  - [Opening the Model](#opening-the-model)
  - [Running a Base Scenario](#running-a-base-scenario)
  - [Creating Alternative Scenarios](#creating-alternative-scenarios)
- [Example: Visualizing Scenario Differences](#example-visualizing-scenario-differences)
- [Documentation](#documentation)
- [References](#references)
- [Contact](#contact)
- [License](#license)

## Overview

PTTMAM is a system dynamics model implemented in Vensim® DSS. It helps policymakers and researchers explore how various factors, such as subsidies and technological advancements, influence the market penetration of different vehicle powertrain technologies, including battery electric vehicles (BEVs), plug-in hybrid electric vehicles (PHEVs), and fuel cell vehicles (FCVs).

## Key Features

- **Market Dynamics Simulation**: Models the adoption and diffusion of different powertrain technologies over time.
- **Scenario Analysis**: Users can create and compare different scenarios by adjusting subsidies and other market parameters.
- **Visualization Tools**: Provides graphical and numerical outputs that show the impact of different scenarios on vehicle market shares and other key indicators.

## Requirements

- **Vensim® DSS**: A licensed version of Vensim® DSS is required to run PTTMAM.

## Installation

1. **Clone the Repository**:
   ```bash
   git clone https://code.europa.eu/dione/pttmam.git
   ```
2. **Set Up the PTTMAM Folder**:
   - Ensure that all files in the PTTMAM folder are kept together. The primary model file is `PTTMAM 02_03c.vpm`.

## Getting Started

### Opening the Model

1. **Open the Model**:
   - Double-click on `PTTMAM 02_03c.vpm`. The simulation run named `Base 02_03c` will load by default.
2. **Explore the Model**:
   - Use the `Subscript Control` box to view different model subscripts.
   - Examine various views of the model from the bottom-left corner of the screen.

### Running a Base Scenario

1. **Navigate to the `Sales, Stocks and Shares` View**:
   - Go to the view named `Sales, Stocks and Shares`.
2. **Select a Variable**:
   - Select the variable `EU28 passenger EV share`. You can use the search function by pressing `Ctrl+F` and typing the variable name.
3. **Visualize Results**:
   - Click on the `Graph` button to visualize the results. Alternatively, click `Table` to see numerical values, which can be exported to Excel® by clicking on the `export window contents` icon.

### Creating Alternative Scenarios

1. **Open the Excel® File**:
   - In the PTTMAM folder, open the Excel® file `Subsidies 02_03c.xlsx`.
2. **Introduce Vehicle Subsidies**:
   - In the `Multiple States` tab, input vehicle subsidies for PHEVs, BEVs, and FCVs (e.g., type in a value of 50% for the years 2019 and 2020).
3. **Rename and Run the Scenario**:
   - Back in Vensim®, rename the scenario (e.g., `alternative`) and press `Simulate`.
4. **Wait for Simulation**:
   - While the simulation is running, the two Excel® files (`Baseline 02_03c.xlsx` and `Subsidies 02_03c.xlsx`) will automatically open. Once the simulation is complete, a new file named `alternative.vdf` will be created in the PTTMAM folder.
5. **Visualize New Results**:
   - In the `Subscript Control` box, select the relevant powertrain (e.g., BEV).
   - Visualize the variable `authorities vehicle subsidy proportion` in the view `Authorities: Vehicle Price Subsidies`.

## Example: Visualizing Scenario Differences

1. **Visualize `EU28 passenger EV share`**:
   - Compare the `Base 02_03c` (red line) and the `alternative` (blue line) runs.

## Documentation

For more detailed information on the PTTMAM, refer to the following sources:
- **Technical Report**: Comprehensive technical details and background information on the model (https://joint-research-centre.ec.europa.eu/scientific-tools-and-databases/powertrain-technology-transition-market-agent-model-pttmam/powertrain-technology-transition-market-agent-model-pttmam_en).

## References

- Harrison, G., Thiel, C., Jones, L. (2016). Powertrain Technology Transition Market Agent Model – An introduction. JRC Technical Report. Joint Research Centre. 
  Available at: https://publications.jrc.ec.europa.eu/repository/handle/JRC100418
- Pruyt, E. (2013). Small System Dynamics Models for BIG Issues. Available at: http://simulation.tbm.tudelft.nl/smallSDmodels/Intro.html
- Radzicki, M.J., Taylor, R.A. (1997). Introduction to System Dynamics - A Systems Approach to Understanding Complex Policy Issues. Available at:
  http://lm.systemdynamics.org/DL-IntroSysDyn/inside.htm
- SDS (2018) Introduction to System Dynamics. System Dynamics Society (SDS). https://www.systemdynamics.org/what-is-sd
- Vensim (2017) Documentation. Ventana Systems Inc. Available at: https://www.vensim.com/documentation/index.html

## Contact

- **Email**: [JRC-LDVS-CO2@ec.europa.eu](mailto:JRC-LDVS-CO2@ec.europa.eu)

## License

This project is licensed under the terms of the European Union Public License (EUPL).
